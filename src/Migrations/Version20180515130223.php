<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180515130223 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE detail_commands ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE detail_commands ADD CONSTRAINT FK_6CE0FAF74584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_6CE0FAF74584665A ON detail_commands (product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE detail_commands DROP FOREIGN KEY FK_6CE0FAF74584665A');
        $this->addSql('DROP INDEX IDX_6CE0FAF74584665A ON detail_commands');
        $this->addSql('ALTER TABLE detail_commands DROP product_id');
    }
}
