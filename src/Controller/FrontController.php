<?php

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Product;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// Controleur qui montre tout le front office du site

class FrontController extends Controller {

    /**
     * @Route("/index")
     * @return Response
     */

    public function index() : Response {

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $product = $repository->findAll();

        $repositoryCategory = $this->getDoctrine()->getRepository(Category::class);
        $category = $repositoryCategory->findAll();

        return $this->render('index.html.twig', [
            'title' => 'Bienvenue dans ma Boutique',
            'product' => $product,
            'category' => $category
        ]);
    }

    /**
     * @Route("/")
     * @return Response
     */

    public function index2() : Response {

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $product = $repository->findAll();

        $repositoryCategory = $this->getDoctrine()->getRepository(Category::class);
        $category = $repositoryCategory->findAll();

        return $this->render('index.html.twig', [
            'title' => 'Bienvenue dans ma boutique',
            'product' => $product,
            'category' => $category
        ]);
    }


    /**
     * @Route("/boutique")
     * @return Response
     */

    public function boutique(): Response {

        return $this->render('frontOffice/boutique.html.twig', [
            'title' => 'Boutique'
        ]);
    }

    /**
     * @Route("/about")
     * @return Response
     */
    public function about(): Response {

        return $this->render('frontOffice/about.html.twig', [
            'title' => 'En savoir plus sur notre Boutique'
        ]);
    }

    /**
     * @Route("contact")
     * @return Response
     */
    public function contact(Request $request, \Swift_Mailer $mailer) {

        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $message = (new \Swift_Message($contact['subject']))
                ->setFrom($contact['email'])
                ->setTo('crownbackend@gmail.com')
                ->setBody(
                    $contact['message'],
                    'text/plain'
                );

            $mailer->send($message);

            $this->addFlash('notice', 'Le méssage a bien étais envoyer vous allez recevoir une réponse d\'ici 48h' );
            return $this->redirectToRoute('app_front_about');
        }

        return $this->render('frontOffice/contact.html.twig', [
            'title' => 'En cas de problème contacter nous',
            'formContact' => $form->createView()
        ]);
    }

    /**
     * @Route("/panier")
     * @return
     */

    public function panier() {

        return $this->render('frontOffice/panier.html.twig', [
            'title' => 'Votre panier'
        ]);
    }

    /**
     * @Route("/detail-produit/{id}")
     * @param int $id
     * @return Response
     */

    public function detailProduit(int $id): Response {

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneWithCategory($id);

        $comment = $this->getDoctrine()->getRepository(Product::class)->findByComment($id);

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $productAnnexe = $repository->findAll();

        return $this->render('frontOffice/detail_produit.html.twig', [
            'title' => 'Detail du produit',
            'product' => $product,
            'productAnnexe' => $productAnnexe,
            'comment' => $comment
        ]);
    }

    /**
     * @Route("/categorie/{id}")
     * @param int $id
     * @return Response
     */

    public function categorie(int $id): Response {

        $category = $this->getDoctrine()->getRepository(Category::class)->findAll($id);

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $product = $repository->findByCategory($id);

        return $this->render('frontOffice/category_product.html.twig', [
           'title' => 'Tout les produis',
           'category' => $category,
           'product' => $product
        ]);
    }

































}