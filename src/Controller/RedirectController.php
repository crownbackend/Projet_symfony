<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends Controller {

    // Controlleur qui permet de faire des rédurection vers les routes


    /**
     * @Route("/accueil")
     * @return Response
     */

    public function index(): Response {

        return $this->redirectToRoute('app_front_index', array(), 301);

    }

    /**
     * @Route("/home")
     * @return Response
     */

    public function index2(): Response {

        return $this->redirectToRoute('app_front_index', array(), 301);

    }

    /**
     * @Route("/en-savoir-plus-sur-nous")
     * @return Response
     */

    public function about(): Response {

        return $this->redirectToRoute('app_front_about', array(), 301);

    }
}