<?php

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Product;
use App\Form\CategoryType;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackController extends Controller {


    /**
     * @Route("/admin/index")
     * @return Response
     */

    public function indexAdmin(): Response {

        $repository = $this->getDoctrine()->getRepository(Product::class);

        $product = $repository->findAll();

        return $this->render('backOffice/index_admin.html.twig', [
            'title' => 'page d\'aministrateur',
            'product' => $product
        ]);
    }

    /**
     * @Route("/admin/detail/{id}")
     * @param int $id
     * @return Response
     */

    public function detail(int $id): Response {

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneWithCategory($id);

        return $this->render('backOffice/CRUD/detail.html.twig', [
            'title' => 'Detail du produit',
            'product' => $product
        ]);

    }


    /**
     * @Route ("/admin/ajout")
     * @return Response
     */

    public function add(Request $request): Response {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $product = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($product);
            $manager->flush();
            $this->addFlash('notice', 'Le produit a bien étais ajouter');

            return $this->redirectToRoute('app_back_indexadmin');

        }

        return $this->render('backOffice/CRUD/add.html.twig', [
            'creatProduct' => $form->createView(),
            'title' => 'Ajouter un produit a votre base de donnée '
        ]);

    }


    /**
     * @Route("admin/ajout-categorie")
     * @return Response
     */

    public function addCategory(Request $request) {

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $category = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($category);
            $manager->flush();
            $this->addFlash('notice', 'La Catégorie a bien étais ajouter !');

            return $this->redirectToRoute('app_back_indexadmin');
        }

        return $this->render('backOffice/CRUD/add_category.html.twig', [
            'creatCategory' => $form->createView(),
            'title' => 'Ajouter une categorie'
        ]);

    }


    /**
     * @Route("/admin/editer/{id}")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editProduct(Request $request, int $id): Response {

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $product = $form->getData();

            $products = $this->getDoctrine()->getManager();
            $products->flush();

            return $this->redirectToRoute('app_back_indexadmin');

        }

        return $this->render('backOffice/CRUD/edit.html.twig', [
            'title' => 'détaille du produit',
            'editProduct' => $form->createView()
        ]);

    }

    /**
     * @Route("/admin/supprimer/{id}")
     * @param int $id
     * @return response
     * @throws \Exception
     */

    public function deleteProduct(int $id) {

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($product);
        $manager->flush();
        $this->addFlash('noticeDelete', 'Le produits a bien étais supprimer');

        return $this->redirectToRoute('app_back_indexadmin');
    }


















}