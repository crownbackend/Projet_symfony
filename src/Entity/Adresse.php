<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseRepository")
 */
class Adresse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $streetNumbre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailCommands", mappedBy="adresse")
     */
    private $detailCommands;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->detailCommands = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getStreetNumbre(): ?string
    {
        return $this->streetNumbre;
    }

    public function setStreetNumbre(string $streetNumbre): self
    {
        $this->streetNumbre = $streetNumbre;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }


    /**
     * @return Collection|DetailCommands[]
     */
    public function getDetailCommands(): Collection
    {
        return $this->detailCommands;
    }

    public function addDetailCommand(DetailCommands $detailCommand): self
    {
        if (!$this->detailCommands->contains($detailCommand)) {
            $this->detailCommands[] = $detailCommand;
            $detailCommand->setAdresse($this);
        }

        return $this;
    }

    public function removeDetailCommand(DetailCommands $detailCommand): self
    {
        if ($this->detailCommands->contains($detailCommand)) {
            $this->detailCommands->removeElement($detailCommand);
            // set the owning side to null (unless already changed)
            if ($detailCommand->getAdresse() === $this) {
                $detailCommand->setAdresse(null);
            }
        }

        return $this;
    }
}
