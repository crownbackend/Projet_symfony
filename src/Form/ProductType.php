<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class, [
                'attr' => [
                    'rows' => '15',
                    'placeholder' => 'Une descrition de mnimum 50 lettre'
                ]
            ])
            ->add('price', NumberType::class, [
                'attr' => [ 'placeholder' => 'Prix en €']
            ])
            ->add('category')
            ->add('imageFile', FileType::class, [
                'attr' => [
                    'label' => 'Ajouter une image',
                    'class' => 'custom-file-input'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class' => Product::class
        ]);
    }
}
